#!/usr/bin/env python

import sys
from setuptools import setup

if sys.version_info < (3, 0):
    sys.exit("Python 2.x is not supported; Python 3.x is required.")

setup(
    name='gitlab-checkout-mr',
    version='0.1',
    python_requires='>=3.6',
    description='GitLab Merge Request Checkout-er',
    #long_description=open('README.md').read(),
    #long_description_content_type='text/markdown',
    author='Daniel Lenski',
    author_email='dlenski@gmail.com',
    py_modules=['gitlab_checkout_mr'],
    install_requires=open('requirements.txt').readlines(),
    entry_points = {
        'console_scripts': [ 'gitlab-checkout-mr=gitlab_checkout_mr:main' ],
    },
    license='GPL v3 or later',
    url='https://gitlab.com/dlenski/gitlab-checkout-mr',
)
