#!/usr/bin/env python3

import argparse
import requests
import re
import subprocess as sp


def main(args=None):
    default_host = default_owner = default_repo = default_gitlab_remote_name = prefer_ssh = None
    gitlab_owner_repo_map = {}
    not_git = None

    try:
        remotes = sp.check_output(['git', 'remote', '-v'], universal_newlines=True).splitlines()
    except sp.CalledProcessError as e:
        if e.returncode != 128:
            raise
        not_git = True
    else:
        # prefer 'origin', prefer ssh remotes over http(s) remotes
        for fields in sorted((line.split() for line in remotes),
                             key=lambda fields: (fields[0] != 'origin', fields[1].startswith('http'), fields)):
            if len(fields) >= 2:
                m = re.search(r'((?:[-A-Za-z0-9]{2,}\.)*[-A-Za-z0-9]{2,})[:/]([^/]+)/([^/]+?)(?:\.git)?$', fields[1])
                if m:
                    host, owner, repo = m.groups()
                    if '.gitlab.' in f'.{host}.':
                        remote_name, remote_uri = (fields[0], fields[1])
                        gitlab_owner_repo_map[(owner, repo)] = (remote_name, remote_uri)
                        if default_owner is None:
                            default_host, default_owner, default_repo, default_gitlab_remote_name = host, owner, repo, remote_name
                            prefer_ssh = not remote_uri.startswith('http')
                            if prefer_ssh and default_host.startswith('ssh.'):
                                default_host = default_host[4:]

    description = ('Checkout a merge request from a GitLab repository. Supports gitlab.com as well as any '
                   'other host that accepts the GitLab V4 RESTful API.')
    if default_owner and default_repo:
        description += f' Found default host, owner and repo from git remote {default_gitlab_remote_name!r}.'

    p = argparse.ArgumentParser(description=description)
    p.add_argument('-H', '--api-host', default=default_host or 'gitlab.com',
                   help='GitLab API host (default %(default)s)')
    p.add_argument('owner', default=default_owner, nargs='?' if default_owner else None,
                   help='GitLab repository owner' + (' (default %(default)s)' if default_owner else ''))
    p.add_argument('repo', default=default_repo, nargs='?' if default_repo else None,
                   help='GitLab repository name' + (' (default %(default)s)' if default_repo else ''))
    p.add_argument('merge_request', type=int,
                   help='Number of the merge-request to check out locally')
    p.add_argument('-b', '--branch',
                   help='Name of the local tracking branch to create (by default, it will follow the name of the branch in the MR)')
    p.add_argument('-t', '--track', action='store_true',
                   help='Set up the local branch to track the remote branch (may require creating a new remote)')
    args = p.parse_args()

    if not_git:
        p.error("Must be run in a Git working directory")

    s = requests.session()
    r = s.get(f'https://{args.api_host}/api/v4/projects/{args.owner}%2F{args.repo}/merge_requests/{args.merge_request}')
    r.raise_for_status()
    mr = r.json()
    r2 = s.get(f"https://{args.api_host}/api/v4/projects/{mr['target_project_id']}")
    r2.raise_for_status()
    trgp = r2.json()
    if mr['source_project_id'] == mr['target_project_id']:
        srcp = trgp
    else:
        r2 = s.get(f"https://{args.api_host}/api/v4/projects/{mr['source_project_id']}")
        r2.raise_for_status()
        srcp = r2.json()
    if args.branch is None:
        args.branch = mr['source_branch']

    print(f'Found MR !{args.merge_request} from GitLab project {args.owner}/{args.repo}')
    print(f'Web: https://{args.api_host}/{args.owner}/{args.repo}/merge_requests/{args.merge_request}')
    print(f"Title: {mr['title']!r}")
    print(f"Target branch: {mr['target_branch']!r} in {trgp['path_with_namespace']}")
    print(f"Source branch: {mr['source_branch']!r} from {srcp['path_with_namespace']}")

    # Figure out if we already have a remote for this source project
    gitlab_remote_name, gitlab_remote_uri = gitlab_owner_repo_map.get((srcp['namespace']['path'], srcp['path']), (None, None))
    if gitlab_remote_uri is None:
        gitlab_remote_uri = srcp['ssh_url_to_repo' if prefer_ssh else 'http_url_to_repo']

    if args.track:
        # Create remote for source project if it doesn't exist
        if gitlab_remote_name is None:
            gitlab_remote_name = srcp['namespace']['path']
            sp.check_call(['git', 'remote', 'add', gitlab_remote_name, gitlab_remote_uri])
            print(f'Created remote {gitlab_remote_name!r} to track {gitlab_remote_uri}')
        print(f'Fetching branch {mr["source_branch"]!r} from remote {gitlab_remote_name!r}, then (re)creating local branch {args.branch!r} to track it')

        # Fetch and check out as tracking branch
        sp.check_call(['git', 'fetch', gitlab_remote_name, mr['source_branch']])
        sp.check_call(['git', 'checkout', '-B', args.branch or mr['source_branch'], gitlab_remote_name + '/' + mr['source_branch']])  # FIXME: -B

    else:
        print('Fetching branch {!r} from {}, then (re)creating local branch {!r} at its head'.format(
            mr['source_branch'], gitlab_remote_uri, args.branch))
        sp.check_call(['git', 'fetch', gitlab_remote_uri, mr['source_branch']])
        sp.check_call(['git', 'checkout', '-B', args.branch, 'FETCH_HEAD'])  # FIXME: -B


if __name__ == '__main__':
    main()
